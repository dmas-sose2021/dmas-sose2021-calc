
CC := gcc
SDIR := source
TSDIR := test
ODIR := obj

SRCS := $(wildcard $(SDIR)/*.c)
#SRCS := $(SRCS2:%.c=source/%.c)
#BINS := $(SRCS:%.c=obj/%.o)

BINS = $(patsubst $(SDIR)/%.c,$(ODIR)/%.o,$(SRCS))

TESTSRC := $(wildcard $(TSDIR)/*.c)
#TESTBINS := $(TESTSRC:%.c=%.o)
TESTBINS = $(patsubst $(TSDIR)/%.c,$(ODIR)/%.o,$(TESTSRC))

BINS2 := $(filter-out $(ODIR)/calculator.o, $(BINS))

$(ODIR)/%.o: $(SDIR)/%.c
	@echo Creating object...
	${CC} -g -c -o $@ $<

$(ODIR)/%.o: $(TSDIR)/%.c
	@echo Creating test object...
	${CC} -g -c -o $@ $<
  
  
calculator: $(BINS)
	@echo Creating executable...
#	@echo "BINS: $(BINS)"
	${CC} $(BINS) -lm -o calculator.exe

clean:
	@echo Cleaning directory...
	rm -f obj/*.o
	find . -type f -name '*.o' -delete

winclean:
	@echo Cleaning directory...
	del /s *.o

check: $(TESTBINS) $(BINS)
	@echo Creating test suite...
#	@echo "SRCS: $(SRCS)"
#	@echo "OBJS: $(OBJS)"
#	@echo "BINS: $(BINS)"
#	@echo "BINS2: $(BINS2)"
#	@echo "TESTSRC: $(TESTSRC)"
#	@echo "TESTBINS: $(TESTBINS)"
	${CC} $(TESTBINS) $(BINS2) -lm -o test.exe
	@echo Executing test suite...
	./test.exe
	