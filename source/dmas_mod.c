#include "dmas_mod.h"

float dmas_mod(float arg1, float arg2)
{
  int tmp = (int)(arg1 / arg2);
  int sum = tmp * arg2;
  return arg1 - sum;
}
