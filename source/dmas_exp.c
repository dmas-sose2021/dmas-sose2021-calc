#include "dmas_exp.h"

// Function to calculate approximate value of e^x

float dmas_exp(float arg1)
{
    int n = 120;  // n terms of Taylor Series
    float sum = 1.0f; // initialize sum of series

    if (arg1 >= 0) {    // condition to check if the argument is not negative
        for (int i = n - 1; i > 0; --i)
            sum = 1 + arg1 * sum / i;
        return sum;
    }
    else {
        for (int i = n - 1; i > 0; --i)
            sum = 1 + (arg1 * (-1)) * sum / i;
        return 1 / sum;
    }
}
