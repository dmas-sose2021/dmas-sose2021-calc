#include <stddef.h>
#include "CuTest.h"
#include "../source/dmas_sqrt.h"


void test_sqrt(CuTest* tc)
{
	float eps = 0.001;
	CuAssertDblEquals( tc, 0.0, dmas_sqrt(0.0), eps );
	CuAssertDblEquals( tc, 1.0, dmas_sqrt(1.0), eps );
	CuAssertDblEquals( tc, 2.0, dmas_sqrt(4.0), eps );
	CuAssertDblEquals( tc, 100.0, dmas_sqrt(10000.0), eps );
	CuAssertDblEquals( tc, 0.9, dmas_sqrt(0.81), eps );
	CuAssertDblEquals( tc, 0.001, dmas_sqrt(0.000001), eps );
}