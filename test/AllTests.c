#include <stdio.h>

#include "CuTest.h"

// declaration of test functions
void test_add(CuTest*);
void test_sqrt(CuTest*);
void test_sqrt_new(CuTest*);
void test_exp(CuTest*);
void test_pow(CuTest*);

CuSuite* CuGetSuite()
{
	CuSuite* suite = CuSuiteNew();

	SUITE_ADD_TEST(suite, test_add);
	SUITE_ADD_TEST(suite, test_sqrt);
	SUITE_ADD_TEST(suite, test_sqrt_new);
	SUITE_ADD_TEST(suite, test_exp);
	SUITE_ADD_TEST(suite, test_pow);

}

int RunAllTests(void)
{
	CuString *output = CuStringNew();
	CuSuite* suite = CuSuiteNew();

	CuSuiteAddSuite(suite, CuGetSuite());

	CuSuiteRun(suite);
	CuSuiteSummary(suite, output);
	CuSuiteDetails(suite, output);
	printf("%s\n", output->buffer);
	return suite->failCount;
}

int main(void)
{
	return RunAllTests();
}
