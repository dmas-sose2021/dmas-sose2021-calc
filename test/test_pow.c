#include <stddef.h>
#include "CuTest.h"
#include "../source/dmas_pow.h"


void test_pow(CuTest* tc)
{
	float eps = 0.001;
	CuAssertDblEquals( tc, 1.0, dmas_pow(0.0, 0.0), eps );
	CuAssertDblEquals( tc, 0.0, dmas_pow(0.0, 1.0), eps );
	CuAssertDblEquals( tc, 1.0, dmas_pow(10.0, 0.0), eps );
	CuAssertDblEquals( tc, 0.0000001, dmas_pow(0.0000001, 1.0), 0.00000001 );
	CuAssertDblEquals( tc, 1.0, dmas_pow(-10.0, 0.0), eps );
	CuAssertDblEquals( tc, -5.0, dmas_pow(-5.0, 1.0), eps );
	CuAssertDblEquals( tc, 2, dmas_pow(4, 0.5), eps );
	CuAssertDblEquals( tc, 1000000, dmas_pow(10, 6), eps );
	CuAssertDblEquals( tc, 0.000001, dmas_pow(0.001, 2), 0.0000001 );
}