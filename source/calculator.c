#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define STR_HEADLINE "DMAS SoSe 2022 calculator V0.31\n"

// include your function header file here
#include "dmas_add.h"
#include "dmas_sqrt.h"
#include "dmas_sqrt_new.h"
#include "dmas_pow.h"
#include "dmas_ln.h"
#include "dmas_tan.h"
#include "dmas_exp.h"
#include "dmas_sin.h"
#include "dmas_cos.h"
#include "dmas_sgn.h"
#include "dmas_division.h"
#include "dmas_mod.h"
typedef union {
    float (*fun2p)(float a, float b);   // function with two arguments
    float (*fun1p)(float a);    		// function with one argument
    float (*fun0p)(void);	    		// function with no argument
} funtype;

struct cmd_s {
    funtype fun; 			    // function with 2-1-0 arguments
    char *name;                 // name of function
    int nr_of_args;             // number of actual arguments (0, 1 or 2)
};

// forward declarations
float myhelp();
float myexit();
float stack_dup();
float stack_swap();
float stack_clear();

struct cmd_s allfuncs[] =
{
    { {.fun0p = myhelp}, "help", 0 },			// user command for help functionality
    { {.fun0p = myexit}, "exit", 0 },			// user command for exiting the program
	{ {.fun0p = stack_swap}, "swap", 0 },		// swap top and second entry on stack
	{ {.fun0p = stack_dup}, "duplicate", 0 },	// duplicate top entry on stack
	{ {.fun0p = stack_clear}, "clear", 0 },	// duplicate top entry on stack
    // add here new functions!
//	{ my_func, "my function name", 1}, // C function name, user function name (as string), number of used args
// 	use {.fun1p = myfunc} for functions with one parameter only
// 	use {.fun0p = myfunc} for functions with no parameter
    { dmas_add, "add", 2 },			// U. Margull, add function
	{ {.fun1p = dmas_sqrt}, "sqrt",1},      // Nivas, sqrt function
	{ {.fun1p = dmas_sqrt_new}, "sqrt_new",1},      // Ronakkumar (sqrt function is changed to 1 argument)
	{ {.fun2p = dmas_pow}, "pow",2},      // Elnagdy, Power function
	{ {.fun1p = dmas_ln}, "ln",1},        // Ronakkumar, Natural log function
	{ {.fun1p = dmas_tan}, "tan",1},	// Advik , Tan function
	{ {.fun1p = dmas_exp}, "exp",1},	// D. Luz , exponential function
	{ {.fun1p = dmas_sin}, "sin",1},	// pradeep , sin function
	{ {.fun1p = dmas_cos}, "cos",1},	// paresh, cosine function
	{ {.fun1p = dmas_sgn}, "sgn",1},	// Markus, sgn function
	{ {.fun2p = dmas_division}, "div",2}, // Veronica, division function
  { {.fun2p = dmas_mod}, "mod",2}, // David, Modulo function
	{ NULL, "", -1 }
};

struct cmd_s * find_cmd(char*fname)
{
    int i = 0;
    while(allfuncs[i].fun.fun0p!=NULL) {
        if( strncmp(fname, allfuncs[i].name, strlen(fname))==0) {
            return &allfuncs[i];
        }
        i++;
    }
    return NULL;
};

// helper functions
float myexit()
{
    printf("Bye bye\n");
    exit(0);
    // not reached
    return 0;
}

float myhelp()
{
    int i = 0;
    printf("Supported commands:");
    while(allfuncs[i].fun.fun0p!=NULL) {
        printf(" %s", allfuncs[i].name );
        i++;
    }
    printf("\n");
    return 0;
}


void error( char* err )
{
	printf("Error: %s\n", err );
	exit(1);
}

int isfnumber( char s[], float*fptr)
{
	if( s==0 ) return 0;
	if( isdigit(s[0]) || (s[0]=='.')  || (s[0]=='+')  || (s[0]=='-') ) {
		sscanf( s, "%f", fptr );
		return 1;
	} else {
		return 0;
	}
}

#define STACK_MAX 100
float stack[STACK_MAX];
int stack_idx = 0;
void stack_push( float v )
{
	if( stack_idx < (STACK_MAX-1) ) {
		stack[stack_idx++] = v;
	} else {
		error("stack_push(): Stack overflow");
	}
}

void stack_pop( float * vptr )
{
	if( (stack_idx > 0) && (vptr!=NULL) ) {
		*vptr = stack[--stack_idx];
	} else {
		error("stack_pop(): Stack empty");
	}
}

float stack_dup()
{
	if( stack_idx==0 ) {
		error("stack_dup(): Stack empty");
	} else if( stack_idx < (STACK_MAX-1) ) {
		stack[stack_idx] = stack[stack_idx-1];
		stack_idx++;
	} else {
		error("stack_dup(): Stack overflow");
	}
	return 0.0;
}

float stack_clear()
{
	stack_idx = 0;
	return 0.0;
}

float stack_swap()
{
	if( stack_idx<2 ) {
		error("stack_swap(): Stack empty or one element only");
	} else {
		float x = stack[stack_idx-1];
		stack[stack_idx-1] = stack[stack_idx-2];
		stack[stack_idx-2] = x;
	}
	return 0.0;
}

int stack_isempty()
{
	return stack_idx == 0;
}

void stack_print()
{
	printf("[ ");
	for( int i = 0; i<stack_idx; i++ )
		printf("%f ", stack[i] );
	printf("]\n");
}

int process_postfix()
{
    char cmd[100];
    struct cmd_s * ptr;
    myhelp(0.0,0.0);
    while(1) {
        float res;
		char* token;
		const char delim[] = " \n";
		int flag_push = 1;
		if( ! stack_isempty() ) {
			stack_print();
		}
        printf("--> ");
        fgets( cmd, 80, stdin );
		token = strtok(cmd, delim );
		while( token != NULL ) {
			float val;
			//printf("token: '%s'\n", token);
			if( isfnumber(token, &val) ) {
				stack_push(val);
			} else {
				ptr = find_cmd(token);
				if( ptr == NULL ) {
					printf("Unknown function name: %s\n", token);
					myhelp(0,0);
				} else {
					float a=0, b=0;
					if( ptr->nr_of_args>0 ) {
						if( stack_isempty() ) {
							flag_push = 0;
							printf("arg1: ");
							scanf("%f",&a);
						} else {
							stack_pop(&a);
						}
						if( ptr->nr_of_args==2 ) {
							if( stack_isempty() ) {
								flag_push = 0;
								printf("arg2: ");
								scanf("%f",&b);
							} else {
								b = a;
								stack_pop(&a);
							}
							res = ptr->fun.fun2p(a,b);
						} else {
							res = ptr->fun.fun1p(a);
						}
					} else {
						res = ptr->fun.fun0p();
					}
					if( ptr->nr_of_args > 0) {
						if( flag_push )
							stack_push( res );
						else
							printf("Result: %f\n", res);
					}
				}
			}
			token = strtok(NULL, delim);
        }
    }
	return 1;
}

int main(int argc, char *argv[])
{
	printf(STR_HEADLINE);
    printf("postfix (polish) notation - try: 3 2 1 add mul\n");
	return process_postfix();
}
