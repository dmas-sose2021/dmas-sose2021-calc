#include<stdio.h>
#include<math.h>
#include "dmas_sin.h"
int fac(int x)  //factorial function used in taylor series
{
	int fac=1;
	for(int i=1;i<=x;i++)
	    fac= fac*i;
	return fac;
}
float dmas_sin(float x) 
{
	float p,sumk=0;
	int i,j;
	p=x;
	x=x*(3.1412/180);
	
	for(i=1,j=1;i<=10;i++,j=j+2) //limit of taylor series has been set to 10
	{
		if(i%2!=0)
		{
			sumk= sumk+pow(x,j)/fac(j);
		}
		else
		{
			sumk=sumk-pow(x,j)/fac(j);
		}
	}
	return sumk;
	
}