#include "dmas_sgn.h"

float dmas_sgn(float arg)
{
	if(arg < 0){return -arg;}
	if(arg == 0) {return 0;}
	return arg;
}