#include "dmas_sqrt_new.h"

float dmas_sqrt_new(float n){  // 'n' is number whoose square root is to be found
    double result;
	double g = 1.0;
    
    while(n>0){
        result = (g + n/g)/2;
        if(result != g){
            g = result;
        }else{
            break;
        }
    }if(n==0){
        result=0;
    }
    
    return result;
    
}