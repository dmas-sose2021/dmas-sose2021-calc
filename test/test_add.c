#include <stddef.h>
#include "CuTest.h"
#include "../source/dmas_add.h"


void test_add(CuTest* tc)
{
	float eps = 0.001;
	CuAssertDblEquals( tc, 0.0, dmas_add(0.0, 0.0), eps );
	CuAssertDblEquals( tc, 1.0, dmas_add(1.0, 0.0), eps );
	CuAssertDblEquals( tc, 1.0, dmas_add(0.0, 1.0), eps );
	CuAssertDblEquals( tc, 2.7, dmas_add(1.5, 1.2), eps );
	CuAssertDblEquals( tc, 500001.0, dmas_add(1.0, 500000.0), eps );
	CuAssertDblEquals( tc, -500001.0, dmas_add(1.0, -500002.0), eps );
}