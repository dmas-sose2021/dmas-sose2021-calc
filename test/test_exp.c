#include <stddef.h>
#include "CuTest.h"
#include "../source/dmas_exp.h"


void test_exp(CuTest* tc)
{
	float eps = 0.001;
	CuAssertDblEquals( tc, 1.0, dmas_exp(0.0), eps );
	CuAssertDblEquals( tc, 2.7182818, dmas_exp(1.0), eps );
	CuAssertDblEquals( tc, 10000, dmas_exp(9.2103), 1.0 );
	CuAssertDblEquals( tc, 0.367879, dmas_exp(-1.0), eps );
	CuAssertDblEquals( tc, 0.0001, dmas_exp(-9.2103), 0.0000001 );
}